cc.Class({
    extends: cc.Component,

    properties: {
        labDec:{
            default: null,
            type: cc.Label
        }
    },

    // use this for initialization
    onLoad: function () {
        
    },

    init :function(data){
        this.labDec.string = data;
    }
    
});

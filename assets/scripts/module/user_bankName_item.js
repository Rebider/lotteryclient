cc.Class({
    extends: cc.Component,

    properties: {
        labBankName:{
            default: null,
            type: cc.Label
        }
    },

    // use this for initialization
    onLoad: function () {

    },

    init: function(data){
        this.labBankName.string = data;
    }
    
});

cc.Class({
    extends: cc.Component,

    properties: {
        
        rtEndTime:{
            default:null,
            type:cc.RichText
        },

        pfOpenItem:{
            default:null,
            type:cc.Prefab
        },

        pfIssueItem:{
            default:null,
            type:cc.Prefab
        },

        pfNumItem:{
            default:null,
            type:cc.Prefab
        },

        pfContentItem:{
            default:null,
            type:cc.Prefab
        },

        ndOpenPanle:{
            default:null,
            type:cc.Node
        },

        ndRedPanle1:{
            default:null,
            type:cc.Node
        },

        ndRedPanle2:{
            default:null,
            type:cc.Node
        },

        ndRedPanle3:{
            default:null,
            type:cc.Node
        },

        ndBluePanle1:{
            default:null,
            type:cc.Node
        },
        ndBluePanle2:{
            default:null,
            type:cc.Node
        },
        ndBluePanle3:{
            default:null,
            type:cc.Node
        },

        spSort:{
            default:[],
            type:cc.SpriteFrame
        },

        ndRedHotPanle:{
            default:null,
            type:cc.Node
        },

        fbHotItem:{
            default:null,
            type:cc.Prefab
        },

        ndBlueHotPanle:{
            default:null,
            type:cc.Node
        },

        swRedContent:{
            default:[],
            type:cc.ScrollView
        },

        swBlueContent:{
            default:[],
            type:cc.ScrollView
        },

        swContent: cc.ScrollView, 
        pftestItem: cc.Prefab,
        tgSort: [cc.Toggle],

        _data:null,
        _openData:[],//tr
        _rDataList:[],//r
        _bDataList:[],//b
        _btDataList:[],//bt
 
        _frontTg:null,
        _isdrowBlueLine:false,
        _reaCount: 18,
        _spacing: 4,
        _totalCount: 50,
        _openItemArr: [],

        _addType: false,
        _addType1: false,
        _addType2: false,
        _addType3: false,
        _addType4: false,
        _addType5: false,
    },

    // use this for initialization
    onLoad: function () {
        var self = this;
        TrentChart.initTrentChart(this._data.Lotteryid,this._data.playCode,function(data){
            self.showPage(data);
        });
    },

    showPage:function(info){
      //  cc.log("doubletrendpage showpage",info);
        this._color = [];
        var color1 = new cc.Color(253, 253, 251);
        var color2 = new cc.Color(246, 247, 241);
        var color =[];
        color.push(color1);
        color.push(color2);
        this._color.push(color1);
        this._color.push(color2);

        var tempinfo = eval('('+ info +')');
        var trdata = tempinfo["tr"]; 
        this._openData = trdata;  
        
        this._totalCount =this._openData.length;
        this.lastPositionY =0;
        this.buffer =740;
        
        if(this._openData.length<=this._reaCount){
            this._reaCount =this._openData.length
        }

        this.ndOpenPanle.parent.parent.getComponent(cc.ScrollView).scrollToTop(0.1);
        for(var i=0;i<this._reaCount;++i){
            var openItem =cc.instantiate(this.pfOpenItem);
            openItem.setPosition(0,-openItem.height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                redNums:this._openData[i].rn,
                blueNums:this._openData[i].bn,
                color:this._color[i%2]
            };
            openItem.getComponent(openItem.name).onItemIndex(i);
            openItem.getComponent(openItem.name).init(data); 
            this.ndOpenPanle.addChild(openItem);
            this._openItemArr.push(openItem);
        }
        this.openItemHeight =openItem.height;
        this.ndOpenPanle.height =this._totalCount*(this.openItemHeight+this._spacing)+this._spacing;
        this.numItemArr =[];
        this.hotItemArr =[];
        this.numItemArr1 =[];
        this.hotItemArr1 =[];
        //红蓝球走势
        this.issueItemArr =[];
        this.contentItemArr =[];
        this.issueItem1Arr =[];
        this.issueItem2Arr =[];
        this.issueItemArr2 =[];
        this.contentItembArr =[];
        for(var i=0;i<this._openData.length;i++)
        {
            //走势
            //期号
            var issueItem = cc.instantiate(this.pfIssueItem);
            issueItem.color = color[i%2];
            issueItem.getChildByName("labIssue").getComponent(cc.Label).string = this._openData[i].i.substring(this._openData[i].i.length-3,this._openData[i].i.length) + "期";;
            this.issueItemArr.push(issueItem);

            var issueItem = cc.instantiate(this.pfIssueItem);
            issueItem.color = color[i%2];
            issueItem.getChildByName("labIssue").getComponent(cc.Label).string = this._openData[i].i.substring(this._openData[i].i.length-3,this._openData[i].i.length) + "期";;
            this.issueItemArr2.push(issueItem);
        }

        //统计出现期数、最大、平均遗漏、最大连出
        var censusStr = ["出现期数","平均遗漏","最大遗漏","最大连出"];
        var censtrCor1 = new cc.Color(122,30,150);
        var censtrCor2 = new cc.Color(37,87,0);
        var censtrCor3 = new cc.Color(110,35,0);
        var censtrCor4 = new cc.Color(0,96,132);
        var censtrCor5 = new cc.Color(229,225,214);
        var censtrCor6 = new cc.Color(234,233,229);
        var colorBg = [censtrCor5,censtrCor6];
        var colorStr = [censtrCor1,censtrCor2,censtrCor3,censtrCor4];
        this.colorBg =[censtrCor5,censtrCor6];
        this.colorStr = [censtrCor1,censtrCor2,censtrCor3,censtrCor4];
        for(var i=0;i<4;i++)
        {
            var issueItem1 = cc.instantiate(this.pfIssueItem);
            issueItem1.color = colorBg[(i+this._openData.length)%2];
            issueItem1.getChildByName("labIssue").getComponent(cc.Label).string = censusStr[i];
            issueItem1.getChildByName("labIssue").color = colorStr[i];
            this.issueItem1Arr.push(issueItem1);

            var issueItem2 = cc.instantiate(this.pfIssueItem);
            issueItem2.color = colorBg[(i+this._openData.length)%2];
            issueItem2.getChildByName("labIssue").color = colorStr[i];
            issueItem2.getChildByName("labIssue").getComponent(cc.Label).string = censusStr[i];
            this.issueItem2Arr.push(issueItem2);
        }
        //red
        var rnData = tempinfo["rn"];
        var raData = tempinfo["ra"];
        var rmData = tempinfo["rm"];
        var rsData = tempinfo["rs"];
        var rnamsList = [];
        rnamsList.push(rnData);
        rnamsList.push(raData);
        rnamsList.push(rmData);
        rnamsList.push(rsData);
        this.rnamsList =[rnData,raData,rmData,rsData];
        //blue
        var bnData = tempinfo["bn"];
        var baData = tempinfo["ba"];
        var bmData = tempinfo["bm"];
        var bsData = tempinfo["bs"];
        var bnamsList = [];
        bnamsList.push(bnData);
        bnamsList.push(baData);
        bnamsList.push(bmData);
        bnamsList.push(bsData);

        for(var i=0;i<rnamsList.length;i++)
        {
            //红
            var contentItem = cc.instantiate(this.pfContentItem);
            var reddata = {
                type:1,
                rt:rnamsList[i],
                color:color[(i+this._openData.length)%2],
                numColor:colorStr[i],
                isSpecial:1,
            }
            contentItem.getComponent(contentItem.name).init(reddata);
            this.numItemArr.push(contentItem);
            //蓝
            var contentItemb = cc.instantiate(this.pfContentItem);
            var reddata = {
                type:2,
                rt:bnamsList[i],
                color:color[(i+this._openData.length)%2],
                numColor:colorStr[i],
                isSpecial:1,
            }
            contentItemb.getComponent(contentItemb.name).init(reddata);
            this.numItemArr1.push(contentItemb);
        }

        //冷热
        //red球号
        var rData = tempinfo["r"];
        var r3Data = tempinfo["r3"];
        var r5Data = tempinfo["r5"];
        var r0Data = tempinfo["r0"];

        for(var i=1;i<34;i++)
        {
            var num = i;
            var numstr = num.toString().length<2?"0"+num.toString():num.toString();
            var data = {
                type:1,
                num:numstr,
                issue30:r3Data[i-1],
                issue50:r5Data[i-1],
                issue0:r0Data[i-1],
                miss:rData[i-1],
                color:color[i%2]
            }
            this._rDataList.push(data);
            var hotItem = cc.instantiate(this.fbHotItem);
            hotItem.getComponent(hotItem.name).init(data);
            this.hotItemArr.push(hotItem);
        }

        //blue球号
        var bData = tempinfo["b"];
        var b3Data = tempinfo["b3"];
        var b5Data = tempinfo["b5"];
        var b0Data = tempinfo["b0"];
        for(var i=1;i<17;i++)
        {
            var num = i;
            var numstr = num.toString().length<2?"0"+num.toString():num.toString();
            //冷热
            var data = {
                type:2,
                num:numstr,
                issue30:b3Data[i-1],
                issue50:b5Data[i-1],
                issue0:b0Data[i-1],
                miss:bData[i-1],
                color:color[i%2]
            }
            this._bDataList.push(data);
            var hotItem = cc.instantiate(this.fbHotItem);
            hotItem.getComponent(hotItem.name).init(data);
            this.hotItemArr1.push(hotItem);
        }
    },

    splitTwo:function(num){
        return num.toString().length<2?"0"+num.toString():num.toString();
    },

    dorwGraphicsLine:function(){

        if(this._isdrowBlueLine)
            return;
        this._isdrowBlueLine = true;

        this.ndBluePanle3.getComponent(cc.Layout)._updateLayout();
        var ctx = this.ndBluePanle3.getComponent(cc.Graphics);

        var curPosX = -1;
        var curPosY = -1;

        var curPosX1 = -1;
        var curPosY1 = -1;
   
        var count = 0;
        var baseY = 296;
        var base = 74;
        
        for(var i=this._btDataList.length-1;i>=0;--i)
        {
            if(count == 0)
            {
                this._btDataList[i].getComponent(cc.Layout)._updateLayout();
            }
            count = 1;
            
            var prenPos = this._btDataList[i].getPosition();
            var childern = this._btDataList[i].getComponent(this._btDataList[i].name).getGraphicsNode();
            var childernPos = childern.getPosition();
            
            curPosX =  childern.x;
            curPosY =  base*(this._btDataList.length-i) + baseY - base/2;
            ctx.moveTo(curPosX,curPosY);
            if(i-1>=0)
            {
                this._btDataList[i-1].getComponent(cc.Layout)._updateLayout();
                var prenPos1 = this._btDataList[i-1].getPosition();
                var childern1 = this._btDataList[i-1].getComponent(this._btDataList[i-1].name).getGraphicsNode();
                var childernPos1 = childern1.getPosition();
                curPosX1 =  childern1.x;
                curPosY1 = base*(this._btDataList.length-i) + baseY + base/2;
                ctx.lineTo(curPosX1,curPosY1);
                ctx.stroke();
            }
        }
    },

    //开奖期号排序
    issueSort:function(toggle){
        var children = this.ndOpenPanle.children;
        var childrenLen = children.length;
        //--------
        this.ndOpenPanle.parent.parent.getComponent(cc.ScrollView).scrollToTop(0.1);
       
        if(toggle.getComponent(cc.Toggle).isChecked){
            this._openData =this._openData.reverse(); 
        }
        else{
            this._openData =this._openData.reverse();
        }
       
        for(var i=0;i<this._openItemArr.length;++i){
            this._openItemArr[i].setPosition(0,- this._openItemArr[i].height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                redNums:this._openData[i].rn,
                blueNums:this._openData[i].bn,
                color:this._color[i%2]
            };
            this._openItemArr[i].getComponent(this._openItemArr[i].name).onItemIndex(i);
            this._openItemArr[i].getComponent(this._openItemArr[i].name).updateData(data); 
        }
    },

    //前区排序
    onfrontSort:function(toggle,customEventData){
        this.hotSort(toggle,this.ndRedHotPanle,this._rDataList,customEventData);
    },

    //后区排序
    onAfterSort:function(toggle,customEventData){
        this.hotSort(toggle,this.ndBlueHotPanle,this._bDataList,customEventData);
    },

    hotSort:function(toggle,panle,arry,key){
        toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[0];
        if(this._frontTg!=null)
            this._frontTg.target.getComponent(cc.Sprite).spriteFrame = this.spSort[0];
        this._frontTg = toggle;

        var children = panle.children;
        var childrenLen = children.length;
        
        var keyStr = "";
        switch (key)
        {
            case "1"://号码
            {
                if(toggle.getComponent(cc.Toggle).isChecked)    
                {
                    Utils.sortByKey(arry,"num",true);
                    if(childrenLen == arry.length)
                    {
                        for (var i = 0; i < childrenLen; ++i) {
                            children[i].getComponent(children[i].name).updateData(arry[i]);
                        }
                    }
                }
                else
                {
                    Utils.sortByKey(arry,"num",false);
                    if(childrenLen == arry.length)
                    {
                        for (var i = 0; i < childrenLen; ++i) {
                            children[i].getComponent(children[i].name).updateData(arry[i]);
                        }
                    }
                    toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[2];
                }
                return;
            }
            break;
            case "2"://30期
            {
                keyStr = "issue30";
            }
            break;
            case "3"://50期
            {
                keyStr = "issue50";
            }
            break;
            case "4"://100期
            {
                keyStr = "issue0";
            }
            break;
            case "5"://遗漏
            {
                keyStr = "miss";
            }
            break;
        }

        if(keyStr != "")
        {
            if(toggle.getComponent(cc.Toggle).isChecked)    
            {
                Utils.sortByKey(arry,keyStr,false);
                if(childrenLen == arry.length)
                {
                    for (var i = 0; i < childrenLen; ++i) {
                        children[i].getComponent(children[i].name).updateData(arry[i]);
                    }
                }
                
            }
            else
            {
                Utils.sortByKey(arry,keyStr,true);
                if(childrenLen == arry.length)
                {
                    for (var i = 0; i < childrenLen; ++i) {
                        children[i].getComponent(children[i].name).updateData(arry[i]);
                    }
                }
                toggle.target.getComponent(cc.Sprite).spriteFrame = this.spSort[1];
            }
        } 
    },

    //分层
    scrollCallBack: function (scrollview, eventType, customEventData) {
        if(eventType === cc.ScrollView.EventType.BOUNCE_BOTTOM)
        {
            var offset_y = this.scrollview.getScrollOffset().y;
            var max_y = this.scrollview.getMaxScrollOffset().y; 
            if(offset_y - max_y>200){
                if(this.isNowRefreshing == false){
                    this.isNowRefreshing = true;
                    this.getResult(this.lotteryId, false);     
                } 
            }
        }
    },


    onScrollowPanle1:function(scrollview, eventType, customEventData){
        var offset = scrollview.getScrollOffset();

        if(customEventData == 1)//red
        {
            this.swRedContent[2].scrollToOffset(offset);
        }
        else if(customEventData == 2)//blue
        {
            this.swBlueContent[2].scrollToOffset(offset);
        }
    },

    onScrollowPanle2:function(scrollview, eventType, customEventData){
        var offset = scrollview.getScrollOffset();
        var offx = Math.abs(offset.x);
        if(customEventData == 1)//red
        {
            this.swRedContent[2].scrollToOffset(cc.p(offx,offset.y));
        }
        else if(customEventData == 2)//blue
        {
            this.swBlueContent[2].scrollToOffset(cc.p(offx,offset.y));
        }
    },

    onScrollowPanle3:function(scrollview, eventType, customEventData){
        var offset = scrollview.getScrollOffset();
        var offx = 0 - offset.x;
        if(customEventData == 1)//red
        {
            this.swRedContent[0].scrollToOffset(offset);
            this.swRedContent[1].scrollToOffset(cc.p(offx,offset.y));
        }
        else if(customEventData == 2)//blue
        {
            this.swBlueContent[0].scrollToOffset(offset);
            this.swBlueContent[1].scrollToOffset(cc.p(offx,offset.y));
        }
    },

    init:function(data,home){
        this._data = data;
        this._home =home;
    },

    update:function(dt){
        var issueStr = TrentChart.getCurIssue();
        if(issueStr != "")
        {
            var currentTimeStamp = Date.parse(new Date())/1000;
            var time = 0;
            var endtime = TrentChart.getEndTime();
            if(endtime == "91")
            {
                this.rtEndTime.string = "<color=#000000>本期已封盘</c>";
                return;
            } 
            else if(endtime == "90")
            {
                this.rtEndTime.string == "<color=#000000>预售中</c>";
                return;
            }
            var leftTimeStamp = endtime - currentTimeStamp;
            if(leftTimeStamp >= 0){
                time = endtime - currentTimeStamp;
            }
            else
            {
                
                this.rtEndTime.string = "<color=#000000>距"+ issueStr +"期投注截止：</c><color=#ff0000>--:--:--</color>";
                return;
            }
            var h = parseInt(time/(60*60)).toString();
            var hStr = (parseInt(h)%24).toString();
            var dStr = parseInt( parseInt(h)/24).toString();
            hStr = hStr.length==1?("0"+hStr):hStr;
            var m = (parseInt((time-h*60*60)/60)).toString();
            m = m.length==1?("0"+m):m;
            var s = (parseInt(time-h*60*60-m*60)).toString();
            s = s.length==1?("0"+s):s;
            var timeStr = "";
            if(parseInt(dStr)>0){
                timeStr = dStr+"天 "+ hStr+":"+m+":"+s;
            }else{
                timeStr = hStr+":"+m+":"+ s;
            }
            this.rtEndTime.string = "<color=#000000>距"+ issueStr +"期投注截止：</c><color=#ff0000>"+ timeStr +"</color>";
        }
        else{
               this.rtEndTime.string = "";
        } 

    },

    onClose:function(){ 
         this.node.getComponent("Page").backAndRemove();
         //父节点显示
         this._home.node.active =true;
         this._addType =false;
         this._addType1 =false;
         this._addType2 =false;
         this._addType3 =false;
         this._addType4 =false;
         this._addType5 =false;
    },

    scrollCallBackFun: function(){
        if(!this._openItemArr.length){
            return;
        }else{ 
            if(this.tgSort[0].isChecked){
                Utils.preNodeComplex(this.ndOpenPanle,this.openItemHeight,this._spacing,this._reaCount,this._openItemArr,this.buffer,this._openData,this._color,'double_open_item');  
            }else if(this.tgSort[1].isChecked){
                Utils.preNodeComplex(this.ndRedPanle3,this.contentItemHeight,this._spacing,this._reaCount,this.contentItemArr,this.buffer,this._openData,this._color,'trend_content_item',1);   
                var maxOffset =this.ndRedPanle3.parent.parent.getComponent(cc.ScrollView).getMaxScrollOffset();
                var offset =this.ndRedPanle3.parent.parent.getComponent(cc.ScrollView).getScrollOffset();
            }else if(this.tgSort[2].isChecked){
                Utils.preNodeComplex(this.ndBluePanle3,this.contentItemHeight,this._spacing,this._reaCount,this.contentItembArr,this.buffer,this._openData,this._color,'trend_content_item',3);    
                var maxOffset =this.ndBluePanle3.parent.parent.getComponent(cc.ScrollView).getMaxScrollOffset();
                var offset =this.ndBluePanle3.parent.parent.getComponent(cc.ScrollView).getScrollOffset();
            }
        }
    },

    //----切换到那个单选按钮，才将它们addChild到舞台上来
    //红球冷热
    tgFront: function(){
        if(this._addType){return}
        for(var i=0;i<this.hotItemArr.length;++i){
            this.ndRedHotPanle.addChild(this.hotItemArr[i]); 
            this._addType =true;  
        }    
    },

    //蓝球冷热
    tgAfter: function(){
        if(this._addType1){return}
        for(var i=0;i<this.hotItemArr1.length;++i){
            this.ndBlueHotPanle.addChild(this.hotItemArr1[i]); 
            this._addType1 =true;
        }    
    },

    //红球走势
    tgRedTrend: function(){
        if(this._addType2){return}
        this.redTrendFunc(this.ndRedPanle3,this.contentItemArr,1);

        for(var i=0;i<this.issueItemArr.length;++i){
            this.ndRedPanle1.addChild(this.issueItemArr[i]); 
            this._addType2 =true;  
        }  
    },

    //蓝球走势
    tgBlueTrend: function(){
        if(this._addType3){return}
        this.redTrendFunc(this.ndBluePanle3,this.contentItembArr,2);

        for(var i=0;i<this.issueItemArr2.length;++i){
            this.ndBluePanle1.addChild(this.issueItemArr2[i]); 
            this._addType3 =true;  
        }  
    },

    redTrendFunc: function(nodeName,trendArr,num){
        //---走势节点的复用
        for(var i=0;i<this._reaCount;++i){
            var contentItem =cc.instantiate(this.pftestItem);
            contentItem.setPosition(0,-contentItem.height*(i+0.5)-this._spacing*(i+1));
            if(num ==1){
                var reddata = {
                    type:1,
                    rt:this._openData[i].rt,
                    color:this._color[i%2],
                    numColor:null,
                    isSpecial:0,
                }
            }else{
                var reddata = {
                    type:2,
                    rt:this._openData[i].bt,
                    color:this._color[i%2],
                    numColor:null,
                    isSpecial:0,
                }
            }
            contentItem.getComponent(contentItem.name).onItemIndex(i);
            contentItem.getComponent(contentItem.name).init(reddata);
            nodeName.addChild(contentItem);
            if(num ==1){
                this.contentItemArr.push(contentItem);
            }else{
                this.contentItembArr.push(contentItem);
            }
        }
        this.contentItemHeight =contentItem.height;
        nodeName.height =this._totalCount*(this.contentItemHeight+this._spacing)+this._spacing; 
    }

});


cc.Class({
    extends: cc.Component,

    properties: {
        topTitle:{
            default:null,
            type:cc.Label
        },
        ruleContent:{
            default:[],
            type:cc.Node
        },
        
        spBetBg:cc.Node,
        _curContent:null
    },

    // use this for initialization
    onLoad: function () {
        this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
            console.log('playrulespage node  TOUCH_END');
            var contentRect = this.spBetBg.getBoundingBoxToWorld();
            var touchLocation = event.getLocation();
            if(cc.rectContainsPoint(contentRect, touchLocation) == false){
                this.onClose();
            }
        }, this);
    },

    init:function(lotteryId){
        this.topTitle.string = LotteryUtils.getLotteryTypeDecByLotteryId(lotteryId)+"玩法规则";

        if(lotteryId.toString() == "101" || lotteryId.toString() == "102"){
            this._curContent = this.ruleContent[0];
            this.ruleContent[0].active = true;   
        }else if(lotteryId.toString() == "201" ){
            this._curContent = this.ruleContent[1];
            this.ruleContent[1].active = true; 
        }else if(lotteryId.toString() == "202" ){
            this._curContent = this.ruleContent[2];
            this.ruleContent[2].active = true; 
        }else if(lotteryId.toString() == "301"){
            this._curContent = this.ruleContent[3];
            this.ruleContent[3].active = true; 
        }else if(lotteryId.toString() == "801"){
            this._curContent = this.ruleContent[4];
            this.ruleContent[4].active = true; 
        }else if(lotteryId.toString() == "901"){
            this._curContent = this.ruleContent[5];
            this.ruleContent[5].active = true; 
        }else{
            return;
        }
    },
    
    onClose:function(event){
        this._curContent.active = false;
        this.node.getComponent("Page").backAndRemove();
    }
    
});

cc.Class({
    extends: cc.Component,

    properties: {
        rtName:{
            default: null,
            type: cc.RichText
        },

        labIssue:{
            default: null,
            type: cc.Label
        },

        labMoney:{
            default: null,
            type: cc.Label
        },

        orderDtailiItem:{
            default:null,
            type:cc.Prefab
        },

        content:{
            default:null,
            type:cc.Node
        },
        
        ndFollow:{
            default:null,
            type:cc.Node
        },

        quickRechargePagePrefab:cc.Prefab,
        ndDetail:cc.Node,
        swCorll:cc.ScrollView,

        _data:null,
        lotteryid:0,
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            var dataBase = this._data.data;
            this.rtName.string = "<color=#ffffff>"+"彩友 "+"</color>"+"<color=#00ff00>"+dataBase.Data.Name+" </c><color=#ffffff>的订单</color>"; 
          
            this.labIssue.string = this._data.IsuseName + " 期";
            var numbers = eval('(' + dataBase.Data.LotNumber + ')');//投注内容
            for(var i=0;i<numbers.length;i++)
            {     
                for(var j=0;j<numbers[i].Data.length;j++)
                {
                    var playcode = numbers[i].PlayCode.toString();
                    this.labMoney.string = "订单金额：" + this._data.Amount + " 元";
                    var content = LotteryUtils.getPlayTypeDecByPlayIdNor(playcode,numbers[i].Data[j].isNorm)+ "  "; 
                    content += numbers[i].Data[j].Bet + "注  ";
                    content += numbers[i].Data[j].Multiple + "倍  ";
                    var amo=0;
                    if(playcode == "90102")
                           amo= parseInt(numbers[i].Data[j].Bet) * parseInt(numbers[i].Data[j].Multiple) * 3;
                        else
                           amo= parseInt(numbers[i].Data[j].Bet) * parseInt(numbers[i].Data[j].Multiple) * 2;

                    content +=  amo.toString()+ "元";

                    var oldNums = "";
                    oldNums = numbers[i].Data[j].Number;

                    this.lotteryid = playcode.substring(0,3);
                    var nums = LotteryUtils.analyseRewardNumber(oldNums,"",playcode,this.lotteryid,false,true);

                    var item = cc.instantiate(this.orderDtailiItem);
                    item.getComponent('chat_detail_item').init({
                        number:nums,
                        dec:content
                    });  
                    this.content.addChild(item); 
                }   
                this.swCorll.scrollToTop(0.1);
            }
        }

        this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
            var contentRect = this.ndDetail.getBoundingBoxToWorld();
            var touchLocation = event.getLocation();
            if(cc.rectContainsPoint(contentRect, touchLocation) == false){
                this.onClose();
            }
        }, this);

    },

    init: function(data){
       this._data = data;
    },

    onClose:function(){
         this.node.getComponent("Page").backAndRemove();
    },

    onFollowBet:function(){
        if(!User.getIsvermify())
        {
            ComponentsUtils.showTips("未登录，不能跟投！");
            return;
        }

        if(this._data.BuyType == 1 )
        {
            ComponentsUtils.showTips("追号暂不支持跟单！");
            return;
        }

        var recv = function(ret){
            ComponentsUtils.unblock();
            var obj = ret.Data;
            if(ret.Code === 0 || ret.Code === 90){//预售中可以购买
                if(this._data.IsuseName !== obj.IsuseNum)
                {
                    var log = this._data.IsuseName+"期已截止";
                    ComponentsUtils.showTips(log);
                    this.onClose();
                    return;
                }

                var balace = User.getBalance();
                var toSerMoney = parseFloat(this._data.Amount);
                if(balace<toSerMoney){
                    var quickPayCallback = function(){
                        //cc.log("quickPayCallback success");
                        this.followbet(obj.BeginTime,obj.EndTime);
                    }.bind(this);
                    var quickRechargePage = cc.instantiate(this.quickRechargePagePrefab);
                    var canvas = cc.find("Canvas");
                    canvas.addChild(quickRechargePage);
                    quickRechargePage.getComponent("lottery_quickRecharge").init(this.lotteryid, this._data.IsuseName,toSerMoney,this._data.BuyType, false,quickPayCallback);
                }
                else{
                    this.followbet(obj.BeginTime,obj.EndTime);
                }
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            LotteryCode:this.lotteryid,
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETRECURRENTISUSE, data, recv.bind(this),"POST");   
        ComponentsUtils.block();
    },
    
    followbet:function(BeginTime,EndTime){
        //POST跟投
        var recv0 = function(ret0){
            ComponentsUtils.unblock();
            if (ret0.Code != 0) {
                ComponentsUtils.showTips(ret0.Msg);
            } else {
                window.Notification.emit("moneyShow",null);
                this.onClose();
            }
        }.bind(this);
            
        var data = {
            UserCode:User.getUserCode(),
            OrderCode:this._data.OrderCode,
            Token:User.getLoginToken(),
            IsuseNum:this._data.IsuseName,
            BeginTime:BeginTime,
            EndTime:EndTime
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.FOLLOWBET, data, recv0.bind(this),"POST");
        ComponentsUtils.block();
    },

});



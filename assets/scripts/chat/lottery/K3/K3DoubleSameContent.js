/*
二同
*/
cc.Class({
    extends: cc.Component,

    properties: {
        singleSameTgList:{
            default:[],
            type:cc.Toggle
        },
        singleDiffTgList:{
            default:[],
            type:cc.Toggle
        },
        checkTgList:{
            default:[],
            type:cc.Toggle
        },

        labSelectedAmount:{
            default:null,
            type:cc.Label
        },
        edMutipleNum:{
            default:null,
            type:cc.EditBox
        },
        edIssueNum:{
            default:null,
            type:cc.EditBox
        },

        labTip:{
            default:null,
            type:cc.Label
        },

        ndBetPanle:{
            default: null,
            type: cc.Node
        },

        //机选清空切换按钮
        tgSelect:{
            default:null,
            type:cc.Toggle
        },

        tgIsStop:{
            default:null,
            type:cc.Toggle
        },


        ndAddAward:{
            default:null,
            type:cc.Node
        },

        BASEMONEY:2,
        _rules:"",//规则
        _lotteryID:0,//彩种id
        _curMoney:0,
        _curBetNum:0,
        _totalisuse:0,

        _singleSameDiceNums:[],
        _singleDiffDiceNums:[],
        _checkDiceNums:[],
        _k3ToggleList:[],

        _isStops:-1,//追号到截止

        _betManage:null,
    },

    initReset:function(){
        if(this.isMiss)
            this._betManage.showK3Miss(this._k3ToggleList,this._lotteryID,this._lotteryID + this._rules[0].toString(),true);
    },

    //退出界面时重置界面
    clearAllBetRecord:function(){
        this.clearAllBetSel();
    },

    // use this for initialization
    onLoad: function () {  
          this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
                var contentRect = this.ndBetPanle.getBoundingBoxToWorld();
                var touchLocation = event.getLocation();
                contentRect.x = contentRect.x -20;
                contentRect.width = 1080;
                if(cc.rectContainsPoint(contentRect, touchLocation) == false){//关闭投注界面
                    this.onClose();
                }
        }, this);

        this._totalisuse = 87;//一天期数
        this.initPanel();
        this._k3ToggleList.push(this.singleSameTgList);
        this._k3ToggleList.push(this.singleDiffTgList);
        this._k3ToggleList.push(this.checkTgList);
        this._betManage = cc.find("Canvas").getChildByName("ChatRoomPage").getChildByName("BetManage").getComponent("BetManage");
    },

    showAddAward:function(data){
        for(var i=0;i<data.length;i++)
        {
            var codestr = data[i].PlayCode.toString();
            var code = codestr.substring(codestr.length-2,codestr.length);
            var index = this._rules.indexOf(code); 
            if(index != -1)
            {
                this.ndAddAward.active = true;
                return;
            }
        }
    },

    initPanel:function(){
        for(var i=0;i<this.singleSameTgList.length;i++)
        {
            var checkEventHandler = new cc.Component.EventHandler();
            checkEventHandler.target = this.node; 
            checkEventHandler.component = "K3DoubleSameContent"
            checkEventHandler.handler = "onSingeCallBack";
            checkEventHandler.customEventData = {num:i+1,type:1};//1同号2不同号
            this.singleSameTgList[i].getComponent(cc.Toggle).checkEvents.push(checkEventHandler);

            var checkEventHandler = new cc.Component.EventHandler();
            checkEventHandler.target = this.node; 
            checkEventHandler.component = "K3DoubleSameContent"
            checkEventHandler.handler = "onSingeCallBack";
            checkEventHandler.customEventData = {num:i+1,type:2};//1同号2不同号
            this.singleDiffTgList[i].getComponent(cc.Toggle).checkEvents.push(checkEventHandler);

            var checkEventHandler = new cc.Component.EventHandler();
            checkEventHandler.target = this.node; 
            checkEventHandler.component = "K3DoubleSameContent"
            checkEventHandler.handler = "oncheckCallBack";
            checkEventHandler.customEventData = i+1;
            this.checkTgList[i].getComponent(cc.Toggle).checkEvents.push(checkEventHandler);
        }
    },

    onSingeCallBack:function(toggle, customEventData){
        var num =  customEventData["num"];
        var type = customEventData["type"];
        if(type == 1)
        {
            if(toggle.getComponent(cc.Toggle).isChecked)
            {
                this._singleSameDiceNums.push(num);
                if(Utils.removeByValue(this._singleDiffDiceNums,num))
                {
                    this.singleDiffTgList[num-1].getComponent(cc.Toggle).isChecked = false;
                }
            } 
            else
            {
                Utils.removeByValue(this._singleSameDiceNums,num);
            }
        }
        else if(type == 2)
        {
            if(toggle.getComponent(cc.Toggle).isChecked)
            {
                this._singleDiffDiceNums.push(num);
                if(Utils.removeByValue(this._singleSameDiceNums,num))
                {
                    this.singleSameTgList[num-1].getComponent(cc.Toggle).isChecked = false;
                }
            } 
            else
            {
                Utils.removeByValue(this._singleDiffDiceNums,num);
            }
        }
        this.updateTgSelect();
        this.checkBet();
    },

    oncheckCallBack:function(toggle, customEventData){
        var num =  customEventData;
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this._checkDiceNums.push(num);
        }
        else
        {
            Utils.removeByValue(this._checkDiceNums,num);
        }
        this.updateTgSelect();
        this.checkBet();
    },

    //初始化
    init: function(lotteryId){
        this._lotteryID = lotteryId;
        this._isStops = -1;
        this._rules = [DEFINE.LOTTERYRULEK3.TWOSAME_DAN,DEFINE.LOTTERYRULEK3.TWOSAME_FU];
    },

    //是否中奖后停止追号
    onIsStops:function(toggle, customEventData){
        if(toggle.isChecked == true)
        {
            this._isStops = 0;
        }
        else
        {
            this._isStops = -1;
        }
    },

    //选择清空切换
    onSelectCallBack:function(toggle){
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this.randomSelectCallBack();
        }
        else
        {
            this.clearAllBetSel();
        }
    },

    updateTgSelect:function(){
        if(this._singleSameDiceNums.length>0 || this._singleDiffDiceNums.length>0 || this._checkDiceNums.length>0)
        {
            this.tgSelect.getComponent(cc.Toggle).isChecked = true;
        }
        else
        {  
            this.tgSelect.getComponent(cc.Toggle).isChecked = false;
        } 
    },

    retmoveByType:function(arr,val){
        for(var i=0; i<arr.length; i++) {
            var value = arr[i].checkEvents[0].customEventData;
            if(value["num"] == val) {
                arr.splice(i, 1);
                return true;
            }
        }
        return false;
    },

    //随机选择
    randomSelectCallBack:function(){
        this.clearAllBetSel();
        var list = [];
        list=this.singleDiffTgList.slice();
        var nums = [];
        var randomRedArray = Utils.getRandomArrayWithArray(this.singleSameTgList, 1);
        var temp = randomRedArray[0].checkEvents[0].customEventData;
        this.retmoveByType(list,temp["num"],"num");
        randomRedArray[0].getComponent(cc.Toggle).isChecked = true;
        this.onSingeCallBack(randomRedArray[0],temp);

        var randomRedArray1 = Utils.getRandomArrayWithArray(list, 1);
        randomRedArray1[0].getComponent(cc.Toggle).isChecked = true;
        var temp1 = randomRedArray1[0].checkEvents[0].customEventData;
        this.onSingeCallBack(randomRedArray1[0],temp1);
        return true;
    },

      //计算
    checkBet:function(){
        var bet = 0;
        var len1 = this._singleSameDiceNums.length;
        var len2 = this._singleDiffDiceNums.length;
        var len3 = this._checkDiceNums.length;

        var bet1 = len1*len2;
        var bet2 = len3;
        bet = bet1+bet2;
       
        var muiple = this.getMutipleAmount();
        this.setShowAmount(muiple,bet);
    },

    //清除所有选择
    clearAllBetSel:function(){
        this._singleSameDiceNums.length = 0;
        this._singleDiffDiceNums.length = 0;
        this._checkDiceNums.length = 0;
        for(var i=0;i<this.singleSameTgList.length;i++)
        {
            this.singleSameTgList[i].getComponent(cc.Toggle).isChecked = false;
            this.singleDiffTgList[i].getComponent(cc.Toggle).isChecked = false;
            this.checkTgList[i].getComponent(cc.Toggle).isChecked = false;
        }
        this._isStops = -1;
        this.tgIsStop.getComponent(cc.Toggle).isChecked = false;
        this.tgSelect.getComponent(cc.Toggle).isChecked = false;

        this.setIssueNum("1");
        this.setMutipleAmount("1");
        this.checkBet();
    },


    //设置金额
    setMoney:function(mon){
        this._curMoney = mon;
    },

    //得到金额
    getMoney:function(){
        return this._curMoney;
    },

    //设置注数
    setBetNum:function(num){
        this._curBetNum = num;
    },

    //得到注数
    getBetNum:function(){
        return this._curBetNum;
    },

    //设置期数
    setIssueNum:function(num){
        if(this._totalisuse >= parseInt(num))
        {
            this.edIssueNum.string = num;
        }
        else
        {
            ComponentsUtils.showTips("最大只能选择87期");
            this.edIssueNum.string = this._totalisuse.toString();
        }
        this.checkBet();
    },

    //得到期数
    getIssueNum:function(){
        var num = parseInt(this.edIssueNum.string);
        if(isNaN(num)){ 
            return 1;
        }else{
            return num;
        }        
    },

       //手动期数
    onEditBoxIssueChanged:function(editbox) {
        if(!Utils.isInt(editbox.string))
        {
            ComponentsUtils.showTips("输入格式错误！");
            editbox.string = "1";
        }
        if(editbox.string == null || editbox.string == "")
        {
            editbox.string = "1";
            ComponentsUtils.showTips("倍数不能为空！");
        }

        var amount = parseInt(editbox.string);
         if(isNaN(amount) || amount<=0 ){
            editbox.string = "1";
        }
  
       this.setIssueNum(editbox.string);
    },

    //设置倍数
    setMutipleAmount:function(mutiple){
         this.edMutipleNum.string = mutiple;
         this.checkBet();
    },

    //获取当前倍数
    getMutipleAmount:function(){
        var amount = parseInt(this.edMutipleNum.string);
        if(isNaN(amount)){ 
            return 1;
        }else{
            return amount;
        }        
    },

   //手动倍数
    onEditBoxMutipleChanged:function(editbox) {
        if(!Utils.isInt(editbox.string))
        {
            ComponentsUtils.showTips("输入格式错误！");
            editbox.string = "1";
        }
        if(editbox.string == null || editbox.string == "")
        {
            editbox.string = "1";
            ComponentsUtils.showTips("倍数不能为空！");
        }

        var amount = parseInt(editbox.string);
        if(isNaN(amount) || amount > 999 || amount<=0 ){
            editbox.string = "1";
        }
  
        this.checkBet();
    },

    //显示投注信息
    setShowAmount:function(mut,bet){
        var issue = this.getIssueNum();
        var money = mut*bet*this.BASEMONEY*issue;
        this.setMoney(money);
        this.setBetNum(bet);
        this.labSelectedAmount.string = "共"+bet+"注"+ mut +"倍"+ issue + "期"+ money+"元";
    },

    //投注信息组合
    dataToJson:function(){
        var objs = [];
        var obj1 = new Object(); 
        var obj2 = new Object(); 
        var arry1 = [];
        var arry2 = [];

        var selBet = 0;
        if(this._checkDiceNums.length>0)
        {
            selBet= this._checkDiceNums.length;
            obj1.PlayCode = parseInt(this._lotteryID + this._rules[1]); 
            var dice1 =  this._checkDiceNums;
            Utils.sortNum(dice1);
            //dice1.sort();
            var nums = "";
            for(var i = 0;i<dice1.length;i++)
            {
                if(nums != "")
                {
                    nums+=",";
                }  
                var diceTemp = dice1[i]*11;
                nums += diceTemp.toString();
            }

            var numArrays = {
                "Multiple":this.getMutipleAmount(),
                "Bet":selBet,
                "isNorm":1,
                "Number":nums
            };
            arry1.push(numArrays);
        }    
        if(this.getBetNum() != selBet)
        {
            obj2.PlayCode = parseInt(this._lotteryID + this._rules[0]); 
            var nums = "";
            var num = "";
            var dice1 =  this._singleSameDiceNums;
             Utils.sortNum(dice1);
           // dice1.sort();
            for(var i = 0;i<dice1.length;i++)
            {
                if(num != "")
                {
                    num+=",";
                }  
                var dan = dice1[i]*11;
                num += dan.toString();
            }
            nums = num;
            num = "";
            var dice2 =  this._singleDiffDiceNums;
             Utils.sortNum(dice2);
           // dice2.sort();
            for(var i = 0;i<dice2.length;i++)
            {
                if(num != "")
                {
                    num+=",";
                }  
                num += dice2[i].toString();
            }
            nums = nums + "|" + num;

            var numArrays = {
                "Multiple":this.getMutipleAmount(),
                "Bet":this.getBetNum()-selBet,
                "isNorm":1,
                "Number": nums
            };
            arry2.push(numArrays);
        }

        obj1.Data = arry1.length >0 ? arry1 : null;
        obj2.Data = arry2.length >0 ? arry2 : null;
         
        if(obj1.Data != null )
        {
            JSON.stringify(obj1);
            objs.push(obj1);
        }
        if(obj2.Data != null )
        {
            JSON.stringify(obj2);
            objs.push(obj2);
        }   

        var json = JSON.stringify(objs);
        cc.log("提交订单：" + json);
        return json;
    },

    //追号组合支付
    chasePay:function(){
        var recv = function(ret){
            ComponentsUtils.unblock(); 
            if(ret.Code === 0)
            {
                var len = ret.Data.length;
                if(len != this.getIssueNum())
                {
                    this.setIssueNum(len);
                    this.setShowAmount();
                }
                var obj = new Object(); 
                obj.Stops = this._isStops;
                obj.IsuseCount = len;
                obj.BeginTime = ret.Data[0].BeginTime;
                obj.EndTime = ret.Data[len-1].EndTime;
                var arry = [];
                for(var i=0;i<ret.Data.length;i++)
                {
                    var numArrays = {
                        "IsuseID":ret.Data[i].IsuseCode,
                        "Amount": LotteryUtils.moneytoClient(this.getMoney()/this.getIssueNum()),//每期金额
                        "Multiple":this.getMutipleAmount(),//每期总倍数
                    };
                    arry.push(numArrays);
                }
                obj.Data = arry;
                var json = JSON.stringify(obj);

                var data = {
                    lotteryId:this._lotteryID,//彩种id
                    dataBase:this.dataToJson(),//投注信息
                    otherBase:json,//追号
                    money:this.getMoney(), 
                    buyType: this.getIssueNum() >1?1:0,//追号
                }

                window.Notification.emit("BET_ONPAY",data);
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            LotteryCode:this._lotteryID,
            Top:this.getIssueNum(),
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETADDTOISUSE, data, recv.bind(this),"POST");   
        ComponentsUtils.block();    
    },

   //关闭界面
    onClose:function(){
        window.Notification.emit("BET_ONCLOSE","");
    },

    //付款
    onPayBtn:function(){
        if(this.getMoney() <= 0)
            return;

        if(this.getIssueNum()<=1)
        {
            var data = {
                lotteryId:this._lotteryID,//彩种id
                dataBase:this.dataToJson(),//投注信息
                otherBase:"",//追加
                money:this.getMoney(),
                buyType: 0,//追号
            }
            window.Notification.emit("BET_ONPAY",data);
        }
        else
        {
            this.chasePay();
        }
    },

    onNextPage:function(){
        window.Notification.emit("BET_NEXTPAGE",1);
    },

    onLastPage:function(){
        window.Notification.emit("BET_NEXTPAGE",-1);
    },

    onMiss:function(toggle){
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this.isMiss = true;
            this._betManage.showK3Miss(this._k3ToggleList,this._lotteryID,this._lotteryID + this._rules[0].toString(),true);
        }    
        else
        {
            this.isMiss = false;
            this._betManage.setK3MissArry(false,this._k3ToggleList,"");
        }
    },

});

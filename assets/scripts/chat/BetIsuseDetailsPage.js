cc.Class({
    extends: cc.Component,

    properties: {
        ndLowContent:{
            default:null,
            type:cc.Node
        },

        ndHighContent:{
            default:null,
            type:cc.Node
        },

        //高频彩
        highItem:{
            default: null,
            type: cc.Prefab
        },
        
        //低频彩
        lowItem:{
            default: null,
            type: cc.Prefab
        },

        labOpenTime:{
            default: null,
            type: cc.Label
        },

        ndReward:{
            default:null,
            type:cc.Node
        },

        ballsPrefab:{
            default: [],
            type: cc.Prefab
        },

        k3Spriteframe:{
            default:[],
            type:cc.SpriteFrame
        },

        labDetailName:{
            default: null,
            type: cc.Label
        },

        ndSwLowContent:{
            default:null,
            type:cc.Node
        },

        labCurMoney:{
            default: null,
            type: cc.Label
        },

        labMoney:{
            default: null,
            type: cc.Label
        },


        _curBalls:null,
        _curItem:null,
        _curDetails:null,

        _data:null,
        _lotteryId:0,
        _isuseDetails:null,
    },

    // use this for initialization
    onLoad: function () {
        this.initPrefab();

        if(this._isuseDetails != null)
        {
            this.labOpenTime.string = this._isuseDetails.isuse;

            if(this._isuseDetails.ballFrames != null )
            {
                var Nums = [];
                var ball = cc.instantiate(this._curBalls);
                for(var i=0;i<this._isuseDetails.ballFrames.length;i++)
                {
                     var value = parseInt(this._isuseDetails.ballFrames[i]);
                     Nums.push(CL.MANAGECENTER.getDiceSpriteFrameByNum(value)); 
                }
               
                ball.getComponent('bet_public_balls').init({
                    spNums: Nums,
                    nums:null,
                });   
                this.ndReward.addChild(ball);  
            }
            else if(this._isuseDetails.ballNums != null)
            {
                var ball = cc.instantiate(this._curBalls);
                ball.getComponent('bet_public_balls').init({
                    spNums: null,
                    nums:this._isuseDetails.ballNums,
                });  
                this.ndReward.addChild(ball);              
            }

            if(this._data == null || this._data.DataAward == null || this._curDetails.active == true)
                return;
            this._curDetails.active = true;    
            this.labCurMoney.string = LotteryUtils.moneytoServer(this._data.TotalSales);
            this.labMoney.string = LotteryUtils.moneytoServer(this._data.WinRollover);
            var content = this.ndSwLowContent;
            for(var i = 0;i<this._data.DataAward.length; i++)
            {
                var item = cc.instantiate(this._curItem);
                 item.getComponent("bet_publicDetails_Item").init({
                    value: LotteryUtils.moneytoServer(this._data.DataAward[i].DefaultMoney),
                    name:this._data.DataAward[i].WinLevel,
                    amount:this._data.DataAward[i].WinBet
                });    
                content.addChild(item);
            }
        }
    },

    initPrefab:function(){
        var type = this._lotteryId.substr(0,1);
        this._curBalls = null;
        var index = 0;
        switch (parseInt(type))
        {
            case 1://k3
            {
               index = 1;
               this._curDetails = this.ndHighContent;
               this._curItem = this.highItem;
               this._curDetails.active = true;
               this._curDetails.getChildByName("K3Content").active = true;
            }
            break;
            case 2://115
            {  
                index = 2;
                this._curDetails = this.ndHighContent;
                this._curItem = this.highItem;
                this._curDetails.active = true;
                if(this._lotteryId == "201")
                    this._curDetails.getChildByName("11x5Content2").active = true;
                else
                    this._curDetails.getChildByName("11x5Content1").active = true;    
            }
            break;
            case 3://ssc
            {
                index = 3;
                this._curDetails = this.ndHighContent;
                this._curItem = this.highItem;
                this._curDetails.active = true;
                this._curDetails.getChildByName("sscContent").active = true;
            }
            break;
            case 8://ssq
            {
                index = 4;
                this._curDetails = this.ndLowContent;
                this._curItem = this.lowItem;
            }
            break;
            case 9://big
            {
                index = 5;
                this._curDetails = this.ndLowContent;
                this._curItem = this.lowItem;
            }
            break;    

        }
        this._curBalls = this.ballsPrefab[index];
    },

    init:function(ret){
        this._data = ret.revData;
        this._lotteryId = ret.ID;
        this._isuseDetails = ret.isuseData;
        this.labDetailName.string = LotteryUtils.getLotteryTypeDecByLotteryId(this._lotteryId) + "-期次详情";
    },

    onKeepOn:function(){
        window.Notification.emit("closeBetResult",null);
        window.Notification.emit("gotoBet",null);
        this.onClose();
    },

    onMore:function(){
        this.onClose();
    },

    onClose:function(){
        this.node.getComponent("Page").backAndRemove();
    }

});

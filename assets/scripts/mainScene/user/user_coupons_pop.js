/**
 * 彩券界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        ndAvaContent:cc.Node,
        ndOverContent:cc.Node,
        fdRedItem:cc.Prefab,
        fdRedNoItem:cc.Prefab,
        edText:cc.EditBox,
        labTgCount1:cc.Label,
        labTgCount2:cc.Label,

        _pageindex1:1,
        _pageindex2:1,
        _pagesize:11,
        _isNowRefreshing:false
    },

    // use this for initialization
    onLoad: function () {
        this._pageindex1 = 1;
        this._pageindex2 = 1;
        this.loadRedPage(true);
    },

    loadRedPage:function(type){
        var recv = function recv(ret) { 
            ComponentsUtils.unblock();
            if(ret.Code == 0 )
            {
                var colorStr = new cc.Color(122,122,122);
                for(var i=0;i<ret.Data.length;i++)
                {
                    var decStr = "";
                    var moneyStr = "";
                    var timeStr = "";
                    var rangeStr = "";
                    if(ret.Data[i].CouponsType == 2)
                    {
                        decStr = "满" + LotteryUtils.moneytoServer(ret.Data[i].SatisfiedMoney) + "减" + LotteryUtils.moneytoServer(ret.Data[i].FaceValue);
                    }
                    else
                    {
                        decStr = LotteryUtils.moneytoServer(ret.Data[i].FaceValue);
                    }
        
                    var startStr = Utils.getYMDbyTime(ret.Data[i].StartTime,"/"); 
                    var endStr = Utils.getYMDbyTime(ret.Data[i].ExpireTime,"/"); 

                    moneyStr = "余额：" + LotteryUtils.moneytoServer(ret.Data[i].Balance) + "元";
                    timeStr = "有效期：" + startStr + " ~ " + endStr;
        
                    if(ret.Data[i].LotteryCode == 0)
                    {
                        rangeStr = "范 围：全场通用";
                    }
                    else
                    {
                        rangeStr = "范 围：" + LotteryUtils.getLotteryTypeDecByLotteryId(ret.Data[i].LotteryCode);
                    }
        
        
                   if(type == true)//可用
                   {
                        var fpRed = cc.instantiate(this.fdRedItem);
                        var data = {
                            dec:decStr,
                            money:moneyStr,
                            time:timeStr,
                            range:rangeStr,
                        }
                        fpRed.getComponent(fpRed.name).init(data);

                        var clickEventHandler = new cc.Component.EventHandler();
                        clickEventHandler.target = this.node; 
                        clickEventHandler.component = "user_coupons_pop"
                        clickEventHandler.handler = "onClickCallBack";
                        clickEventHandler.customEventData = ret.Data[i].LotteryCode;
                        fpRed.getComponent(cc.Button).clickEvents.push(clickEventHandler);
                        this.ndAvaContent.addChild(fpRed);
                   }
                   else//不可用
                   {
                        var fpRedNo = cc.instantiate(this.fdRedNoItem);
                        var data = {
                            dec:decStr,
                            money:moneyStr,
                            time:timeStr,
                            range:rangeStr,
                            state:ret.Data[i].Balance>0?"过期":"用完"
                        }
                        fpRedNo.getComponent(fpRedNo.name).init(data);
                        this.ndOverContent.addChild(fpRedNo);
                    }
                }

                if(type == true)//可用
                {
                    this.labTgCount1.string = "可用（"+ret.Counts+"）";
                    this.labTgCount2.string = "可用（"+ret.Counts+"）";
                    this._pageindex1++;
                }
                else
                {
                    this._pageindex2++;
                }
                this._isNowRefreshing = false;
            }
            else
            {
                this._isNowRefreshing = false;
                if(ret.Code == 49)
                    return;
                ComponentsUtils.showTips(ret.Msg);
            }

         }.bind(this);

        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            PageIndex:type==true?this._pageindex1:this._pageindex2,
            PageSize:this._pagesize,
            IsCoupons:type,
        };
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETCOUPONSLIST, data, recv.bind(this),"POST");    
        ComponentsUtils.block();
    },

    onClickCallBack:function(event, customEventData){
    //    cc.log("前往id:"+customEventData);

        if(customEventData == 0)
        {
            window.Notification.emit("onGotoHall","");
        }   
        else
            window.Notification.emit("HallUI_toGotoRoom",customEventData);

        this.onClose();    
    },

    //滚动获取下一页
    scrollCallBack: function (scrollview, eventType,customEventData) {
        if(eventType === cc.ScrollView.EventType.BOUNCE_BOTTOM)
        {
        //    cc.log("彩券列表：BOUNCE_BOTTOM");
           
            var offset_y = scrollview.getScrollOffset().y;
            var max_y = scrollview.getMaxScrollOffset().y; 
            if(offset_y - max_y>200){
                if(this._isNowRefreshing == false){
                    
      //              cc.log("彩券列表：刷新");
                    this._isNowRefreshing = true;
                    var type = customEventData==1?true:false;
                    this.loadRedPage(type);
                } 
            }
        }
    },

    onTextChange:function(text, editbox, customEventData){
        var txt = text;
        
        if(txt != "")
        {
            if(Utils.isNumAndAbc(txt))
            {
                this.text = txt;
                return;
            }
            else
            {
                editbox.string = this.text;
            }
        }
    },

    //兑换
    onExchange:function(){
        if(this.edText.string.length >=12)
        {
            var recv = function recv(ret) { 
                ComponentsUtils.unblock();
                if(ret.Code == 0 )
                {
                    ComponentsUtils.showTips("兑换成功！");
                    this.onAvailable();
                }
                else
                {
                    ComponentsUtils.showTips(ret.Msg);
                }
    
             }.bind(this);
    
            var data = {
                Token:User.getLoginToken(),
                UserCode:User.getUserCode(),
                CDKey:this.edText.string
            };
            CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETEXCHANGECOUPONS, data, recv.bind(this),"POST");    
            ComponentsUtils.block();
        }
        else
            ComponentsUtils.showTips("输入兑换码不正确！");

    },

    //可用
    onAvailable:function(){
        this.ndAvaContent.removeAllChildren();
        this._pageindex1 = 1;
        this.loadRedPage(true);
    },

    //过期
    onOverdue:function(){
        this.ndOverContent.removeAllChildren();
        this._pageindex2 = 1;
        this.loadRedPage(false);
    },

    onClose:function(){
        window.Notification.emit("USER_useruirefresh","");
        this.node.getComponent("Page").backAndRemove();
    },

});
